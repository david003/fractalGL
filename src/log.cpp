#include <log.hpp>
#include <glm/gtc/quaternion.hpp>

std::ostream& operator<< (std::ostream& os, const glm::vec3& v)
{
	os << std::fixed << std::setprecision(LOG_PRECISION); //set floats to fixed precision
	os << "vec3("; //print header
	for (int i = 0; i < 3; ++i)
	{
		os.width(LOG_PRECISION + 4); // space values and align them right
		os << v[i];
	}
	os << " )"; //close vector
	return os;
}

std::ostream& operator<< (std::ostream& os, const glm::vec4& v)
{
	os << std::fixed << std::setprecision(LOG_PRECISION); //set floats to fixed precision
	os << "vec4("; //print header
	for (int i = 0; i < 4; ++i)
	{
		os.width(LOG_PRECISION + 4); // space values and align them right
		os << v[i];
	}
	os << " )"; //close vector
	return os;
}

std::ostream& operator<< (std::ostream& os, const glm::mat4& mat)
{
	os << std::fixed << std::setprecision(LOG_PRECISION); //set floats to fixed precision
	for (int i = 0; i < 4; ++i)
	{
		os << (i==0? "mat4(" : "     "); //print either header or equivalent spacing
		for (int j = 0; j < 4; ++j)
		{
			os.width(LOG_PRECISION + 4); // space values and align them right
			os << mat[j][i];
		}

		if (i < 3) //if not last line
			os << "\n" << std::setw(LOG_INDENT) << ""; //print newline and indentation
		else
			os << " )"; //close matrix
	}
	return os;
}

std::ostream& operator<< (std::ostream& os, const glm::quat& q)
{
	os << std::fixed << std::setprecision(LOG_PRECISION); //set floats to fixed precision
	os << "quat("; //print header
	for (int i = 0; i < 4; ++i)
	{
		os.width(LOG_PRECISION + 4); // space values and align them right
		os << q[i];
	}
	os << " )"; //close vector
	return os;
}
