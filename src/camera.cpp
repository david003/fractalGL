#include <camera.hpp>
#include <GLFW/glfw3.h>
// #include <glm/gtc/quaternion.hpp>
#include <log.hpp>

Camera::Camera(const glm::vec3& position, const glm::quat& orientation)
:
	m_position(position),
	m_orientation(orientation),
	m_translations(0),
	m_rotations(0)
{}

void Camera::checkForKeys(const int& key, const int& action)
{
	// if(action == GLFW_PRESS && key == GLFW_KEY_RIGHT)
	// 	move(glm::angleAxis(3.1415f / 8, glm::vec3(0,1,0)));
	// return;


	int pressed = 0;
	if(action == GLFW_PRESS) pressed = 1;
	if(action == GLFW_RELEASE) pressed = -1;
	// if(m_invertedView) pressed = -pressed;
	switch(key)
	{
	case GLFW_KEY_W: //forward
		m_translations += glm::vec3(0,0,-pressed); break;
	case GLFW_KEY_S: //backward
		m_translations += glm::vec3(0,0, pressed); break;
	case GLFW_KEY_A: //left
		m_translations += glm::vec3(-pressed,0,0); break;
	case GLFW_KEY_D: //right
		m_translations += glm::vec3( pressed,0,0); break;
	case GLFW_KEY_SPACE: //up
		m_translations += glm::vec3(0, pressed,0); break;
	case GLFW_KEY_LEFT_CONTROL: //down
		m_translations += glm::vec3(0,-pressed,0); break;

	case GLFW_KEY_Q: //roll counterclockwise (-z)
		m_rotations += glm::vec3(0,0,-pressed); break;
	case GLFW_KEY_E: //roll clockwise (+z)
		m_rotations += glm::vec3(0,0,+pressed); break;
	case GLFW_KEY_UP: //pitch up (-x)
		m_rotations += glm::vec3(-pressed,0,0); break;
	case GLFW_KEY_DOWN: //pitch down (+x)
		m_rotations += glm::vec3( pressed,0,0); break;
	case GLFW_KEY_LEFT: //yaw left (-y)
		m_rotations += glm::vec3(0,-pressed,0); break;
	case GLFW_KEY_RIGHT: //yaw right (+y)
		m_rotations += glm::vec3(0, pressed,0); break;
	default:
		//do nothing
		break;
	}

	// std::cout << "key action: " << key << std::endl;
}

void Camera::checkForMouse(GLFWwindow *window)
{
	// return;

	static double xposOld = 0, yposOld = 0;
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	float moveX = (xpos - xposOld) * c_mouseSensitivity;
	float moveY = (ypos - yposOld) * c_mouseSensitivity;
	xposOld = xpos;
	yposOld = ypos;

    static bool initialized = 0;
    if (! initialized) {
    	initialized = true;
    	return;
    }
	// m_rotations = glm::vec3(moveX, moveY, 0);

	glm::quat rot = glm::angleAxis(moveX, glm::vec3(0,1,0)) * glm::angleAxis(moveY, glm::vec3(1,0,0));
	m_orientation = rot * m_orientation;
}

void Camera::move(const glm::vec3& translation, const glm::quat& rotation)
{
	glm::quat orConj = glm::conjugate(m_orientation);
	m_position += orConj * translation * c_translationSpeed;
	m_orientation = rotation * m_orientation;
}

void Camera::move(const glm::quat& rotation)
{
	move(glm::vec3(0), rotation);
}

void Camera::move()
{
	glm::quat rot = glm::angleAxis(m_rotations[0] * c_rotationSpeed, glm::vec3(1,0,0))
	              * glm::angleAxis(m_rotations[1] * c_rotationSpeed, glm::vec3(0,1,0))
	              * glm::angleAxis(m_rotations[2] * c_rotationSpeed, glm::vec3(0,0,1));
	move(m_translations, rot);
}

glm::mat4 Camera::getViewMatrix() const
{
	// LOG("orientation", m_orientation);
	// LOG("rotations", m_rotations);
	return glm::translate(glm::mat4_cast(m_orientation), -m_position);
}
