//Intreaktive Berechnung dreidimensionaler Fraktale

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <chrono>
#include <camera.hpp>
#include <log.hpp>

// #define WIDTH 1367
// #define HEIGHT 740
#define HEIGHT 720
#define WIDTH 1280

const float ASPECT = WIDTH/(float)HEIGHT;
static unsigned int resDivider = 4;
static const int nSubdiv = 1; //number of triangle pairs per row/column

static Camera camera;
static const bool c_disableCursor = false;
static bool skipFrame = false;

static const std::string g_fractalShaderFile = "../shaders/mandelbox2.glsl";
static const std::string g_textureShaderFile = "../shaders/copyTexture.glsl";
static GLuint g_fractalShader, g_textureShader;
static GLuint g_texture, g_framebuffer, g_depthRB;

static float g_box_scale = 2.25;
static int g_box_iter = 30;


struct ShaderSource
{
	std::string vertex_src;
	std::string fragment_src;
};

static void glClearErrors()
{
	while(glGetError() != GL_NO_ERROR);
}

static void glCheckErrors()
{
	while(GLenum error = glGetError())
	{
		std::string type;
		switch(error)
		{
		case 0x0500: type="invalid enum"; break;
		case 0x0501: type="invalid value"; break;
		case 0x0502: type="invalid operation"; break;
		case 0x0503: type="stack overflow"; break;
		case 0x0504: type="stack underflow"; break;
		case 0x0505: type="out of memory"; break;
		case 0x0506: type="invalid framebuffer operation"; break;
		case 0x0507: type="context lost"; break;
		case 0x8031: type="table too large"; break;
		default: type="UNKNOWN";
		}
		LOG(LogLevel::Error) << "OpenGL Error " << error << ":\t" << type << std::endl;
		// std::cout << "[OpenGL Error] (" << error << "):\t" << type << std::endl;
	}
}

static void time(std::string msg = std::string("elapsed time: "), bool flush = false)
{
	static int sum = 0;
	static int count = 0;
	if (flush)
	{
		LOG(LogLevel::Warning) << "average: " << sum/(double)count;
		std::clog<< sum/(double)count << std::endl;
		sum = 0;
		count = 0;
		return;
	}

	auto end = std::chrono::steady_clock::now();
	static auto begin = std::chrono::steady_clock::now();
	int ms = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count();
	LOG() << msg << ms << "ms\n";
	begin = end ;
	std::cout.flush();

	sum += ms;
	++count;
}

static void setupTexture()
{
glClearErrors();
// LOG("setupTexture() begin...", LogLevel::Warning);
	//unbind and delete texture
		// LOG("old texture id", g_texture);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &g_texture);

	//create texture
	glGenTextures(1, &g_texture);
	glBindTexture (GL_TEXTURE_2D, g_texture);
		// LOG("new texture id", g_texture);

	//unbind Framebuffer 0, bind g_framebuffer instead
	//this is needed in order for glFramebufferTexture2D not to fail
	glBindFramebuffer(GL_FRAMEBUFFER, g_framebuffer);

	//assign texture to FBO
	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_texture, 0);
	glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT, WIDTH/resDivider, HEIGHT/resDivider);

	//bind framebuffer 0, aka main window FB
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	// Set Parameters
	glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//specify resolution to be fobRes
	glTexImage2D  (GL_TEXTURE_2D, 0, GL_RGB, WIDTH/resDivider, HEIGHT/resDivider, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
// LOG("setupTexture() end.", LogLevel::Warning);
glCheckErrors();
}

static void error_callback(int error, const char* description)
{
	// fprintf(stderr, "Error: %s\n", description);
	LOG(description, LogLevel::Error);
}

static ShaderSource parseShader(const std::string& filename)
{
	std::ifstream stream(filename);

	enum ShaderType {
		NONE = -1, VERTEX = 0, FRAGMENT = 1
	};

	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	while( getline(stream, line) )
	{
		if(line.find("----VERTEX----") != std::string::npos)
			type = ShaderType::VERTEX;
		else if(line.find("----FRAGMENT----") != std::string::npos)
			type = ShaderType::FRAGMENT;
		else if(line.find("----NONE----") != std::string::npos)
			type = ShaderType::NONE;
		else
		{
			if(type != ShaderType::NONE)
				ss[(int)type] << line << '\n';
		}
	}

	//std::cout << "VERTEX SHADER"   << std::endl << ss[0].str() << std::endl;
	//std::cout << "FRAGMENT SHADER" << std::endl << ss[1].str() << std::endl;

	return {ss[0].str(), ss[1].str()};
}

static unsigned int compileShader(unsigned int type, const std::string& source)
{
	unsigned int id = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if(result == GL_FALSE)
	{
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* message = new char[length];
		glGetShaderInfoLog(id, length, &length, message);
		LOG(LogLevel::Error) << "Failed to compile "
					 << (type==GL_VERTEX_SHADER ? "vertex" : "fragment")
					 << " shader!" << std::endl;
		LOG(LogLevel::Error) << message << std::endl;
		exit(1);
	}

	return id;
}

static unsigned int createShader(const std::string& vertex_shader, const std::string& fragment_shader)
{
	unsigned int program = glCreateProgram();
		time("TIME RESET ");
	unsigned int vs = compileShader(GL_VERTEX_SHADER, vertex_shader);
	unsigned int fs = compileShader(GL_FRAGMENT_SHADER, fragment_shader);
		time("Compiled shaders in ");

	glAttachShader(program, vs);
	glAttachShader(program, fs);

	glLinkProgram(program);
		time("Linked program in ");
	glValidateProgram(program);
		time("Validated Program in ");

	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}

static void loadFractalShaders()
{
	LOG("loading fractal shaders...");
	ShaderSource shader_src = parseShader(g_fractalShaderFile);
	glDeleteProgram(g_fractalShader);
	g_fractalShader = createShader(shader_src.vertex_src, shader_src.fragment_src);
}

static void loadTextureShaders()
{
	LOG("loading texture shaders...");
	ShaderSource shader_src = parseShader(g_textureShaderFile);
	glDeleteProgram(g_textureShader);
	g_textureShader = createShader(shader_src.vertex_src, shader_src.fragment_src);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	LOG("registered keypress!", LogLevel::Warning);
	camera.checkForKeys(key, action);

	if(action == GLFW_RELEASE) return;
	switch(key)
	{
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, GLFW_TRUE);
			skipFrame = true;
			break;
		case GLFW_KEY_T:
			g_box_scale += 0.01;
			skipFrame = true;
			break;
		case GLFW_KEY_G:
			g_box_scale -= 0.01;
			skipFrame = true;
			break;
		case GLFW_KEY_ENTER:
			loadFractalShaders();
			skipFrame = true;
			break;
		case GLFW_KEY_Y:
			g_box_iter += 5;
			skipFrame = true;
			LOG("iterations: ", g_box_iter);
			time("", true);
			break;
		case GLFW_KEY_H:
			g_box_iter -= 5;
			skipFrame = true;
			LOG("iterations: ", g_box_iter);
			time("", true);
			break;
		case GLFW_KEY_Z:
			if(resDivider < (WIDTH/2) && resDivider < (HEIGHT/2))
				resDivider += 1;
			setupTexture();
			skipFrame = true;
			break;
		case GLFW_KEY_X:
			if(resDivider > 1) resDivider -= 1;
			setupTexture();
			LOG("resDivider: ", resDivider);
			skipFrame = true;
			break;
		case GLFW_KEY_SPACE:
			skipFrame = true;
			break;
		default:
			break;
	}
}

static void setupContext()
{
	//triangle setup
	// glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	static const float posAbs[12] = {	//two triangles which fill the whole screen
		 1.0f,  1.0f,
		-1.0f,  1.0f,
		-1.0f, -1.0f,
		-1.0f, -1.0f,
		 1.0f, -1.0f,
		 1.0f,  1.0f
	};
	static const int nPosSize = nSubdiv*nSubdiv*12;
	float positions[nPosSize];
	for(int i=0; i<nSubdiv*nSubdiv; ++i)
	{
		for (int k = 0; k < 12; ++k)
		{
			// LOG() << "i=" << i << " k=" << k << std::endl;
			float p = posAbs[k] / nSubdiv; //scale down
			if(k%2==0)
			{ //all x-coordinates
				p -= (1 - 1.f/nSubdiv); //move to upper left
				p += (i%nSubdiv) * 2.f/nSubdiv; //x from L to R for each elem in set of nSubdiv
			}
			else
			{ //all y-coordinates
				p += (1 - 1.f/nSubdiv); //move to upper left
				p -= (i/nSubdiv) * 2.f/nSubdiv; //y from top to bottom grouped by nSubDiv
			}
			// LOG() << "p=" << p << std::endl;
			// LOG() << "writing into positions[" << (i*12 + k) << "]..." << std::endl << std::endl;
			positions[i*12 + k] = p;
		}
	}
	{
		//this is needed to make the code below work correctly in openGL 3.1+
		GLuint vertBuffer;
		glGenVertexArrays(1, &vertBuffer);
		glBindVertexArray(vertBuffer);
	}
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, nPosSize*sizeof(float), positions, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);


	// Create Frame- and Renderbuffer
	glGenFramebuffers  (1, &g_framebuffer);
	glBindFramebuffer  (GL_FRAMEBUFFER, g_framebuffer);
	glGenRenderbuffers (1, &g_depthRB);
	glBindRenderbuffer (GL_RENDERBUFFER, g_depthRB);
	glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, g_depthRB);

	setupTexture();

	//shader setup
	loadTextureShaders();
	loadFractalShaders();
}

static GLFWwindow* setupWindow()
{
	GLFWwindow *window;
	glfwSetErrorCallback(error_callback);

	/*init glfw lib*/
	if(!glfwInit()) exit(EXIT_FAILURE);

	/*create window and opengl context*/
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	window = glfwCreateWindow(WIDTH, HEIGHT, "fractalGL", NULL, NULL);
	if(!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	if(c_disableCursor) glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	glfwSwapInterval(1);

	LOG() << "Using OpenGL version " << glGetString(GL_VERSION) << std::endl;
	LOG("aspect ratio", ASPECT);

	return window;
}

int main(int argc, char const *argv[])
{
	GLFWwindow *window = setupWindow();

	setupContext();

	// auto startClock = std::clock();
	// auto endClock   = std::clock();
	//loop until user closes window
	while( !glfwWindowShouldClose(window) )
	{
		// endClock = std::clock();
		// LOG("time per frame in seconds:", (endClock - startClock) / (double) CLOCKS_PER_SEC);
		// startClock = std::clock();
		time("frametime: ");

		///////////////////////////////////////////////////////////////////////////////////////////
		///  draw fractal in framebuffer
		///////////////////////////////////////////////////////////////////////////////////////////
		glBindFramebuffer  (GL_FRAMEBUFFER, g_framebuffer);

		// clear frame buffer
		glClearColor (1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Disable texture
		glActiveTexture (GL_TEXTURE0 + 0);
		glBindTexture   (GL_TEXTURE_2D, 0);
		glViewport (0, 0, WIDTH/resDivider, HEIGHT/resDivider);

		//move camera
		camera.checkForMouse(window);
		camera.move();
		glm::mat4 viewMatrix = camera.getViewMatrix();

		auto viewMatrixInverse = glm::inverse(viewMatrix);
		// manual view matrices
		{
			//for debugging
			float array[16] = {
        	-1.0, -0.0, 0.0, -0.0,
        	 0.0, -1.0, 0.0,  0.0,
        	 0.0,  0.0, 1.0,  0.0,
        	 0.0,  0.0, 1.7,  1.0
			};
			float array2[16] = {
			-0.928, -0.075, -0.364, -0.666,
			 0.012, -0.985,  0.173, -0.014,
			-0.371,  0.156,  0.915,  5.934,
			 0.000,  0.000, -0.000,  1.000
        	};
        	float array3[16] = {
			 0.778,  0.388, -0.493, -6.030,
			 0.131, -0.869, -0.477, -2.285,
			-0.614,  0.307, -0.727, -6.015,
			-0.000,  0.000,  0.000,  1.000
			};
			float array4[16] = { //bottom right corner
				0.027, -0.759, -0.651, -2.927,
                0.712, -0.443,  0.545,  2.999,
               -0.702, -0.478,  0.528,  2.972,
               -0.000,  0.000, -0.000,  1.000
			};
			// viewMatrixInverse = glm::make_mat4(array);
			// viewMatrixInverse = glm::transpose(glm::make_mat4(array2));
			// viewMatrixInverse = glm::transpose(glm::make_mat4(array3));
			// viewMatrixInverse = glm::transpose(glm::make_mat4(array4));
		}

		// activate shader
		glUseProgram(g_fractalShader);

		GLint uniVInv = glGetUniformLocation(g_fractalShader, "uni_viewMatrixInverse");
		glUniformMatrix4fv(uniVInv, 1, GL_FALSE, glm::value_ptr(viewMatrixInverse));

		GLint uniScale = glGetUniformLocation(g_fractalShader, "uni_box_scale");
		glUniform1fv(uniScale, 1, &g_box_scale);

		GLint uniIter = glGetUniformLocation(g_fractalShader, "uni_box_iter");
		glUniform1iv(uniIter, 1, &g_box_iter);

		GLint uniAspect = glGetUniformLocation(g_fractalShader, "uni_aspect");
		glUniform1fv(uniAspect, 1, &ASPECT);

		////LOGGING////
		{
			// LOG("uniVInv: ", uniVInv, LogLevel::Warning);
			// LOG("uniScale: ", uniScale, LogLevel::Warning);

			// std::cout << std::setfill('-') << std::setw(80) << "" << std::setfill(' ') << std::endl;
			// LOG("inverse view matrix", viewMatrixInverse);
			// glm::vec4 origin = viewMatrixInverse * glm::vec4(0,0,0,1);
			// LOG("origin", origin);
			// glm::vec4 waypointOff = viewMatrixInverse * glm::vec4(0.5,0.5,-1,1);
			// LOG("waypoint off", waypointOff);
			// glm::vec4 waypointStraight = viewMatrixInverse * glm::vec4(0,0,-1,1);
			// LOG("waypoint straight", waypointStraight);
			// glm::vec4 directionStep = glm::normalize(waypointStraight - origin);
			// LOG("direction step", directionStep);

			// static unsigned int framecount = 0;
			// LOG("frame counter:", framecount++);
			// std::cout << std::setfill('-') << std::setw(80) << "" << std::setfill(' ') << std::endl;
		}

		//draw loop
		for (int i = 0; i < nSubdiv*nSubdiv; ++i)
		{
			glDrawArrays(GL_TRIANGLES, 6*i, 6);
			glfwPollEvents();
			if(skipFrame) break;
		}
		skipFrame = false;
		// glDrawArrays(GL_TRIANGLES, 0, nSubdiv*nSubdiv*6);


		///////////////////////////////////////////////////////////////////////////////////////////
		/// draw texture onto screen
		//////////////////////////////////////////////////////////////////////////////////////////
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, WIDTH, HEIGHT);

		// clear frame buffer
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Enable texture
		glActiveTexture(GL_TEXTURE0 + g_texture);
		glBindTexture  (GL_TEXTURE_2D, g_texture);
		// Set Parameters
		glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		//activate shader
		glUseProgram(g_textureShader);

		GLint uniTex = glGetUniformLocation(g_textureShader, "tex");
		glUniform1i(uniTex, g_texture);

		////LOGGING////
		{
			// LOG("uniTex: ", uniTex, LogLevel::Warning);
			// LOG("g_texture: ", g_texture, LogLevel::Warning);

			// static unsigned int framecount = 0;
			// LOG() << "frame counter:" << framecount++ << std::endl;
			// std::cout << std::setfill('-') << std::setw(80) << "" << std::setfill(' ') << std::endl;
		}

		glDrawArrays(GL_TRIANGLES, 0, nSubdiv*nSubdiv*6);

		glfwSwapBuffers(window);
		glfwPollEvents();
	// glCheckErrors();
	glClearErrors();
	}

	glDeleteProgram(g_fractalShader);
	glDeleteProgram(g_textureShader);
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
