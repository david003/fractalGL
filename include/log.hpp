#pragma once

#include <iostream>
#include <iomanip>
#include <glm/glm.hpp>

#define LOG Logger::log //convenience macro

// #define LOG_ANSI_COLORS //use ansi coloring for log messages. fucks a bit with indentation!
// #define LOG_USE_CERR //use cerr for error output instead of cout
#define LOG_INDENT 12 //indentation for log message
#define LOG_PRECISION 3 //precision for floats

enum LogLevel
{
	Info = 0,
	Warning = 1,
	Error   = 2
};

std::ostream& operator<< (std::ostream& os, const glm::vec3& v);
std::ostream& operator<< (std::ostream& os, const glm::vec4& v);
std::ostream& operator<< (std::ostream& os, const glm::mat4& mat);
std::ostream& operator<< (std::ostream& od, const glm::quat& q);

class Logger
{
public:
	static inline std::ostream& log(const LogLevel& l = LogLevel::Info)
	{
		std::ostream& os = logHeader(l);
		os.unsetf(std::ios::right);
		return os;
	}

	template <typename T>
	static inline void const log(const T& msg, const LogLevel& l = LogLevel::Info)
	{
		std::ostream& os = logHeader(l); //push header to ostream and get handle
		os << msg << std::endl;   //output message
		os.unsetf(std::ios::right); //revert flags
	}

	template <typename A, typename B>
	static inline void const log(const A& msg1, const B& msg2, const LogLevel& l = LogLevel::Info)
	{
		std::ostream& os = logHeader(l); //push header to ostream and get handle
		os << msg1 << "\n"; //output first message after header
		os << std::setw(LOG_INDENT) << ""; //print indentation
		os << msg2 << std::endl;   //output second message
		os.unsetf(std::ios::right); //revert flags
	}

private:
	static inline std::ostream& logHeader(const LogLevel& l)
	{
		//disable output
		std::cout.setstate(std::ios_base::failbit);

		#ifdef LOG_USE_CERR
			std::ostream& os = (l==LogLevel::Error? std::cerr : std::cout);
		#else
			std::ostream& os = std::cout;
		#endif

		os.flags(std::ios::left); //align text left
		// os.fill('.'); //indent with '.' instead of spaces for debugging

		#ifdef LOG_ANSI_COLORS
			os << std::setw(LOG_INDENT + 9); //pad next field to LOG_INDENT number of characters + 9 for color codes
			os << std::string("[") + std::string(l==0 ? "\033[32mInfo\033[0m" : (l==1 ? "\033[33mWarning\033[0m" : "\033[31mError\033[0m")) + std::string("]:");
		#else
			os << std::setw(LOG_INDENT); //pad next field to LOG_INDENT number of characters
			os << std::string("[") + std::string(l==0 ? "Info" : (l==1 ? "Warning" : "Error")) + std::string("]:");
		#endif

		os.setf(std::ios::right); //align text right from now on
		return os;
	}
};
