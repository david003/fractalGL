#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

class GLFWwindow;

class Camera
{
public:
	Camera(const glm::vec3& position = glm::vec3(0, 0, 10), const glm::quat& orientation = glm::quat(0,0,0,1));

	void checkForKeys(const int& key, const int& action);
	void checkForMouse(GLFWwindow *window);

	glm::mat4 getViewMatrix() const;
	// void getViewMatrix(glm::mat4* out_mat) const;

	void move();
	void move(const glm::quat& rotation);
	void move(const glm::vec3& translation, const glm::quat& rotation = glm::quat());

private:
    glm::vec3 m_position; //camera position in world coordinates
    glm::quat m_orientation; //rotation from standard to current orientation
    glm::vec3 m_translations; //translations currently in progress
    glm::vec3 m_rotations; //rotations currently in progress

	const float c_translationSpeed = 0.1; //in units/frame
	const float c_rotationSpeed    = 0.5; //in radiants/frame
	const float c_mouseSensitivity = 0.01; //in mouseDistances/frame
};
