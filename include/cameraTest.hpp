#if 0
#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#define WIDTH 680
#define HEIGHT 680

class GLFWwindow;

class Camera
{
public:
	Camera();
	void move();
	void checkForKeys(int key, int action);
	void checkForMouse(GLFWwindow *window);
	glm::mat4 getViewMatrix();

private:
	void move(glm::vec3 directions, glm::vec2 rotations, float frametime = 1);

	glm::mat4 m_view;
	glm::vec3 m_cameraPos;
	glm::quat m_cameraOrientation;
	const float c_cameraSpeed = 0.1;
	const float c_rotationSpeed = 0.003;
	bool m_invertedView = false;

	glm::vec3 m_movements;
	glm::vec2 m_rotations;
};
#endif
