**fractalGL** - a realtime Mandelbox explorer

# REQUIREMENTS

- This is only known to work on Linux Mint and Manjaro Linux! Other Linux distributions will most likely work just fine.
- It *may* work on Windows or macOS, but it's not guaranteed.
- Nvidia and Intel GPUs should work fine. AMD ist not testet.
- Execute `install_dependencies.sh`. This will clone the two needed libraries from github.
- You need to have g++, cmake and OpenGL installed.

# COMPILATION

Execute `build.sh`. This will build and run the application.

(You will need to run this script twice when compiling for the first time.)

# KEYBOARD LAYOUT

The following table assumes a US (QWERTY) keyboard layout.

| KEY         | FUNCTION                                          |
|-------------|---------------------------------------------------|
| Esc         | quit the application                              |
| Enter       | reload the shaders                                |
|             |                                                   |
| W           | move forward                                      |
| S           | move backward                                     |
| A           | strafe left                                       |
| D           | strafe right                                      |
| Space       | move upwards                                      |
| Ctrl        | move downwards                                    |
|             |                                                   |
| Q           | roll counterclockwise                             |
| E           | roll clockwise                                    |
| Mouse       | look around (tilt and pan)                        |
| ArrowUp     | tilt up                                           |
| ArrowDown   | tilt down                                         |
| ArrowLeft   | pan left                                          |
| ArrowRight  | pan right                                         |
|             |                                                   |
| X           | increase resolution (**be cautious with this!!**) |
| Z           | decrease resolution                               |
| T           | increase maximum iteration count                  |
| G           | decrease maximum iteration count                  |
| Y           | increase scale factor                             |
| H           | decrease scale factor                             |
