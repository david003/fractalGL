----VERTEX----
#version 330 core

in vec4 position;
uniform mat4 mvp;
uniform float uni_aspect;
out vec4 var_rayWaypoint;

void main()
{
	vec2 pos = position.xy;
	//correct for aspect ratio
	pos.y /= uni_aspect;
	//position reaches from -1 to 1 for the two main triangles
	//make sure z is -1, which eqals 90deg fovy
	var_rayWaypoint = vec4(pos, -1, 1);

	// gl_Position = mvp * position;
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec3 out_Color;
in vec4 var_rayWaypoint; //position of ray-waypoint in camera coordinates
uniform mat4 uni_viewMatrixInverse; //inverse view matrix
uniform float uni_box_scale;
uniform int uni_box_iter;

const int c_maxSteps = 1000; //max steps for rays
// const float c_maxIterations = 10; //max iterations per step
float c_maxIterations = uni_box_iter; //max iterations per step

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float mandelboxDist(vec3 v)
{
	//distance estimtaion
	float DEfactor = 1;

	//mandelbox parameters
	const float fixedRadius = 1;
	const float fR2 = fixedRadius*fixedRadius;
	const float minRadius = 0.5;
	const float mR2 = minRadius*minRadius;
	// const float scale = 2;
	float scale = uni_box_scale;

	vec3 it = vec3(0);
	vec3 pos = v;

	// iteration loop for mandelbox
	for(int k=0; k<c_maxIterations; ++k)
	{
		//box fold
		it = clamp(it, -1, 1)*2 -it;

		// sphere fold
		float r2 = dot(it, it);

		if (r2 < mR2)
		{
			float r = fR2 / mR2;
			it *= r;
			DEfactor *= r;
		}
		else if (r2 < fR2)
		{
			float r = fR2 / r2;
			it *= r;
			DEfactor *= r;
		}

		//scaling
		it = it*scale + pos;
		DEfactor = DEfactor * scale + 1;

		//saves 40% to 50% of calculation in exchange for barely noticable uncertainty
		if(abs(DEfactor)> 1000000) break;

	}

	return sqrt(it.x*it.x+it.y*it.y+it.z*it.z)/abs(DEfactor);
}

void main()
{
	const float maxDist = 100.0;
	const float minDist = 0.0001;

	//the vector of the current position
	vec3 pos = (uni_viewMatrixInverse * vec4(0,0,0,1)).xyz; //init ray to camera position in world coordinates
	vec3 waypoint_wc = (uni_viewMatrixInverse * var_rayWaypoint).xyz; //waypoint in world coordinates
	vec3 directionNorm = normalize(waypoint_wc - pos); //direction vector in unit length


	//marching ray
	vec3 march = pos;
	float marchedDist = 0;
	float currDist = maxDist;
	int steps = c_maxSteps;

	for (; steps >= 0; steps--) {
		currDist = mandelboxDist(march);
		if (currDist < minDist) break;

		marchedDist += currDist;
		march = pos + directionNorm * marchedDist;
	}

		if (currDist < minDist)
		{
			vec3 v;
			// v = vec3(mod(steps, marchedDist)); //just white??
			// v =  vec3(float(steps)/c_maxSteps, marchedDist/maxDist, float(steps)/c_maxSteps); //purple
			// v = hsv2rgb(vec3(mod(steps, 1), float(steps)/c_maxSteps, 1)); //red-white
			// v = hsv2rgb(vec3(minDist/currDist, 1, float(steps)/c_maxSteps)); //red-orange
			// v = hsv2rgb(vec3(float(steps)/c_maxSteps, 1, 1)); //psychedelic
			v =  vec3(float(steps)/c_maxSteps); //standard b/w coloring
			// v = vec3(1); //two-tone coloring
			// v *= directionNorm;
			out_Color = v;
		}
		else
		{
			// out_Color = directionNorm;
			out_Color = vec3(0, 0, 0);
		}

	// out_Color.r = 0.6;
}
