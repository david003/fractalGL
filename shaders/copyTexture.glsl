----VERTEX----
#version 330 core

in vec4 position;
out vec2 pos;

void main()
{
	pos = (position.xy + vec2(1,1))/2; //normalize position to [0,1]
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec4 outColor;
in vec2 pos;
uniform sampler2D tex;

void main()
{
	outColor = texture(tex, pos);
}
