----VERTEX----
#version 330 core

in vec4 position;
uniform mat4 mvp;
out vec3 pos;

void main()
{
	pos = (position.xyz + vec3(1,1,1))/2; //normalize position to [0,1]
	// gl_Position = mvp * position;
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec4 outColor;
in vec3 pos;

void main()
{
	outColor = vec4(pos, 1.0);
}