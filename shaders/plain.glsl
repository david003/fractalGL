#version 120
attribute vec4 vertex_position;
uniform mat4 mvp;
void main()
{
   gl_Position = mvp * vertex_position;
}
---
#version 120
uniform vec4 color;
void main()
{
   gl_FragColor = color;
}
