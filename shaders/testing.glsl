----NONE----
This is a testing shader!
It is meant to include the same raymarching as the fractal shader,
but without actually claculating any fractal.
Instead it will just show a sphere.

----VERTEX----
#version 330 core

in vec4 position;
uniform mat4 mvp;
uniform float uni_aspect;
out vec4 var_rayWaypoint;

void main()
{
	vec2 pos = position.xy;
	//correct for aspect ratio
	pos.y /= uni_aspect;
	//position reaches from -1 to 1 for the two main triangles
	//make sure z is -1, which eqals 90deg fovy
	var_rayWaypoint = vec4(pos, -1, 1);

	// gl_Position = mvp * position;
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec3 out_Color;
in vec4 var_rayWaypoint; //position of ray-waypoint in camera coordinates
uniform mat4 uni_viewMatrixInverse; //inverse view matrix

const int c_maxSteps = 1000; //max steps for rays

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float sphereDist(vec3 v)
{
	//radius pf the sphere, squared
	const float sphereRad2 = 9.f;

	float dist2 = dot(v,v) - sphereRad2;
	// out_Color = vec3(-dist2/1);

	if(dist2 >= 0.f) return 1.f; //Emulate a fixed step size!
	if(dist2 < 0.f) return 0.f;
	// return sqrt(dist2); //variable step size
}

void main()
{
	const float maxDist = 100.0;
	const float minDist = 0.0001;

	//the vector of the current position
	vec3 pos = (uni_viewMatrixInverse * vec4(0,0,0,1)).xyz; //init ray to camera position in world coordinates
	vec3 waypoint_wc = (uni_viewMatrixInverse * var_rayWaypoint).xyz; //waypoint in world coordinates
	vec3 directionNorm = normalize(waypoint_wc - pos); //direction vector in unit length


	//marching ray
	vec3 march = pos;
	float marchedDist = 0;
	float currDist = maxDist;
	int steps = c_maxSteps;

	for (; steps >= 0; steps--) {
		currDist = sphereDist(march);
		if (currDist < minDist) break;

		marchedDist += currDist;
		march = pos + directionNorm * marchedDist;
	}

	if (currDist < minDist)
	{
		out_Color = vec3(1);
		//out_Color = vec3((c_maxSteps - float(steps))/c_maxSteps * 900);
	}
	else
	{
		out_Color = directionNorm;
		// out_Color = vec3(0);
	}
}
