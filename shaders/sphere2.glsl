----VERTEX----
#version 330 core

in vec4 position;
uniform mat4 mvp;
out vec4 var_startPos;
out vec4 var_endPos;

void main()
{
	//make sure z is -+1, which eqals nearp/farp after projection
	var_startPos = vec4(position.xy, -1, 1);
	var_endPos   = vec4(position.xy,  1, 1);

	// gl_Position = mvp * position;
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec4 out_Color;
in vec4 var_startPos; //position of ray on nearp
in vec4 var_endPos;   //position of ray on farp
in mat4 in_ViewProjectionInv; //inverse view and projection matrix

const int c_maxIteration = 10000;

struct Sphere
{
	vec3 origin;
	float radius;
};

struct Ray
{
	vec3 start;
	vec3 end;
	vec3 directionStep;
	float totalLength;
	float stepsize;
};

void main()
{
	Ray r;
	r.start = (in_ViewProjectionInv * var_startPos).xyz;
	r.end   = (in_ViewProjectionInv * var_endPos).xyz;

	r.directionStep = r.start - r.end;
	r.totalLength = length(r.directionStep);
	r.stepsize = 1/r.totalLength;
	normalize(r.directionStep);


	{
		Sphere s;
		s.origin = vec3(10,0,0);
		s.radius = 300;

		vec3 r.pos = r.start;
		float r.step = 0;
		for(int i=0; i<c_maxIteration && r.step<totalLength; ++i, r.step+=r.stepsize)
		{
			r.pos += r.directionStep;
			if( length(r.pos - s.origin) < s.radius ) out_Color = vec4(0,1,0,1);
		}
		out_Color = vec4(1,0,0,1);
	}

	// out_Color = vec4( (var_endPos.xyz + vec3(1,1,1))/2,  1);
}
