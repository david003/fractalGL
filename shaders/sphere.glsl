----VERTEX----
#version 330 core

in vec4 position;
uniform mat4 mvp;
out vec4 var_rayWaypoint;

void main()
{
	//make sure z is +1, which eqals 90deg fovy
	var_rayWaypoint = vec4(position.xy, -1, 1);

	// gl_Position = mvp * position;
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec4 out_Color;
in vec4 var_rayWaypoint; //position of ray-waypoint in camera coordinates
uniform mat4 uni_viewMatrixInverse; //inverse view matrix

const int c_maxIteration = 50; //max steps for rays
const float c_stepDivisor = 5.f;

struct Sphere
{
	vec3 origin;
	float radius;
};

struct Ray
{
	vec3 position;
	vec3 directionStep;
};

void main()
{
	Ray r;

	//init ray
	r.position = (uni_viewMatrixInverse * vec4(0,0,0,1)).xyz;
	// r.position = vec3(0,0,0);

	vec3 waypoint_wc = (uni_viewMatrixInverse * var_rayWaypoint).xyz;
	r.directionStep = normalize(waypoint_wc - r.position) / c_stepDivisor;
	// r.directionStep = normalize(var_rayWaypoint.xyz - r.position) / c_stepDivisor;


	//cast ray
	Sphere s;
	s.origin = vec3(0.0f,0.0f,0.0f);
	s.radius = 0.55f;

	for(int i=0; i<c_maxIteration; ++i)
	{
		r.position += r.directionStep;
		if( length(r.position - s.origin) < s.radius )
		{
			out_Color = vec4(0, r.position.z, 0, 1);
			// out_Color = vec4(0,1,0,1);
			return;
		}
	}

	// out_Color = vec4(1,0,0,1);
	out_Color = vec4(normalize(r.directionStep),1);
	// out_Color = vec4( (var_rayWaypoint.xyz + vec3(1,1,0))/2,  1);
	// out_Color = vec4(r.position/20+vec3(0.5,0.5,0), 1);
}




----NONE----

#version 430 core

uniform ivec2 viewportDimensions;
uniform mat4 gl_ProjectionMatrix;
uniform ivec4 viewport;
uniform float imageAspectRatio;
uniform float angle;

out vec3 color;

struct Ray {
	vec3 origin;
	vec3 direction;
};

struct Sphere {
	vec3 origin;
	float radius;
};

bool intersect(Ray r, Sphere s) {
	float a = dot(r.direction,r.direction);
	float b = dot(r.direction, 2.0 * (r.origin-s.origin));
	float c = dot(s.origin, s.origin) + dot(r.origin,r.origin) +-2.0*dot(r.origin,s.origin) - (s.radius*s.radius);

	float disc = b*b + (-4.0)*a*c;

	if (disc < 0)
		return false;

	return true;

}

void main() {

	focal = 60;
	angle = tan(focal * 0.5 * 3.1415926535897932384626433832795 / 180); // convert from degree to radian
	float xx = (2 * (gl_FragCoord.x + 0.5) / viewportDimensions.x - 1) * angle * imageAspectRatio;
	float yy = (1 - 2 * (gl_FragCoord.y + 0.5) / viewportDimensions.y) * angle;

	vec3 rayOrigin = vec3(0,0,0);
	vec3 rayDirection = vec3(xx, yy, -1) - rayOrigin;
	normalize(rayDirection);

	Ray r;
	r.origin = rayOrigin;
	r.direction = rayDirection;

	Sphere s;
	s.origin = vec3(0.0f,0.0f,-1.1f);
	s.radius =0.55f;


	if (intersect(r,s))
		color = vec3(1,0,1);
	else
		color = vec3(0,0,0);
}
