----VERTEX----
#version 330 core

in vec4 position;
uniform mat4 mvp;
out vec4 var_rayWaypoint;

void main()
{
	//make sure z is +1, which eqals 90deg fovy
	var_rayWaypoint = vec4(position.xy, 1, 1);

	// gl_Position = mvp * position;
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec4 out_Color;
in vec4 var_rayWaypoint; //position of ray-waypoint in camera coordinates
uniform mat4 in_viewMatrixInverse; //inverse view matrix

const int c_maxIteration = 10; //max steps for rays
const float c_stepDivisor = 1.f;

struct Sphere
{
	vec3 origin;
	float radius;
};

struct Ray
{
	vec3 position;
	vec3 directionStep;
};

float pow(float a, int b)
{
	for(int i=0; i<b; ++i)
		a *= a;
	return a;
}

vec3 power(vec3 v, int n)
{
	float r = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	float theta = atan( sqrt(v.x*v.x +v.y*v.y), v.z );
	float phi = atan(v.y,v.x);

	return (pow(r, n) * vec3( sin(theta*n) * cos(phi*n) , sin(theta*n) * sin(phi*n) , cos(theta*n) ));
}

void main()
{
	Ray r;

	//init ray
	r.position = (vec4(0,0,0,1) * in_viewMatrixInverse).xyz;
	r.position = vec3(0,0,0);

	vec3 waypoint_wc = (var_rayWaypoint * in_viewMatrixInverse).xyz;
	r.directionStep = normalize(waypoint_wc - r.position) / c_stepDivisor;
	// r.directionStep = normalize(var_rayWaypoint.xyz - r.position) / c_stepDivisor;


	//cast ray
	for(int i=0; i<c_maxIteration; ++i)
	{
		r.position += r.directionStep;
		for
		(	vec3 pos = vec3(0,0,0);
			4 < (pos.x*pos.x + pos.y*pos.y + pos.z*pos.z);
			pos = power(pos, 8) + r.position
		)
		{
			out_Color = vec4(r.position/20+vec3(0.5,0.5,0), 1);
		}
	}


	out_Color = vec4(1,0,0,1);
	// out_Color = vec4( (var_rayWaypoint.xyz + vec3(1,1,0))/2,  1);
	// out_Color = vec4(r.position/20+vec3(0.5,0.5,0), 1);
	// out_Color = vec4(normalize(r.directionStep),1);
}
