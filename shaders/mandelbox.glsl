----VERTEX----
#version 330 core

in vec4 position;
uniform mat4 mvp;
out vec4 var_rayWaypoint;

void main()
{
	//make sure z is -1, which eqals 90deg fovy
	var_rayWaypoint = vec4(position.xy, -1, 1);

	// gl_Position = mvp * position;
	gl_Position = position;
}


----FRAGMENT----
#version 330 core

out vec4 out_Color;
in vec4 var_rayWaypoint; //position of ray-waypoint in camera coordinates
uniform mat4 uni_viewMatrixInverse; //inverse view matrix
uniform float uni_box_scale;

const int c_maxSteps = 2; //max steps for rays
const float c_stepDivisor = 0.1f; //step size
const float c_maxIterations = 20; //max iterations per step

///mandelbox parameters
// float c_box_s = uni_box_scale; //scale
const float c_box_s = 2; //scale
const float c_box_minR = 0.5;
const float c_box_minRSqr = c_box_minR*c_box_minR;
const float c_box_fixedR = 1;
const float c_box_fixedRSqr = c_box_fixedR*c_box_fixedR;
const float c_box_f = 1;
float g_box_DEFactor = 0;

struct Ray
{
	vec3 position;
	vec3 directionStep;
	vec3 directionNorm;
};

float absEuclidSqr(vec3 v)
{
	return (v.x*v.x + v.y*v.y + v.z*v.z);
}

vec3 absEach(vec3 v)
{
	return vec3(abs(v[0]), abs(v[1]), abs(v[2]));
	// return max(max(abs(v[0]), abs(v[1]) ), abs(v[2]));
}

float maxComponent(vec3 v)
{
	return max(max(v[0], v[1]), v[2]);
}

vec3 boxIteration(vec3 z, vec3 pos)
{
	//box fold
	for(int i=0; i<3; i++)
	{
		if (z[i] > 1)
			z[i] = 2 - z[i];
		else if (z[i] < -1)
			z[i] = -2 - z[i];
	}
	z *= c_box_f;

	//sphere fold
	float rSqr = absEuclidSqr(z);
	if (rSqr < c_box_minRSqr)
	{
		z = z * (c_box_fixedRSqr / c_box_minRSqr);
		g_box_DEFactor *= (c_box_fixedRSqr / c_box_minRSqr);
	}
	else if (rSqr < c_box_fixedRSqr)
	{
		z = z * (c_box_fixedRSqr / rSqr);
		g_box_DEFactor *= (c_box_fixedRSqr / rSqr);
	}

	g_box_DEFactor = g_box_DEFactor * abs(c_box_s) + 1;
	// g_box_DEFactor *= c_box_s;
	return (c_box_s * z) + pos;
}

void main()
{
	Ray r;
	r.position = (uni_viewMatrixInverse * vec4(0,0,0,1)).xyz; //init ray to camera pos
	vec3 waypoint_wc = (uni_viewMatrixInverse * var_rayWaypoint).xyz; //waypoint in world coordinates
	r.directionNorm = normalize(waypoint_wc - r.position); //vector from r.pos to waypoint in unit length
	r.directionStep = r.directionNorm / c_stepDivisor; //vector from r.pos to waypoint in step length

	//cast ray
	for(int i=0; i<c_maxSteps; ++i) //step the ray c_max times
	{
		//iterate point
		int k=0; //number of iterations
		vec3 it = vec3(0); //it is the vector to iterate
		g_box_DEFactor = 1;
		while(k<c_maxIterations) //to maximum c_maxIteration times
		{
			k++; //count number of iterations
			it = boxIteration(it, r.position); //do one iteration of formula
			// if(absEuclidSqr(it) < 6) break; //check if already out of bounds
			// if(absEuclidSqr(it) > 50) break; //check if already out of bounds
		}
		// if(k<c_maxIterations) //if escaped bounds earlier
		// if(absEuclidSqr(it) > 200) //if escaped bounds earlier
		{
			// out_Color = vec4(0, k/c_maxIterations, 0, 1);
			// out_Color = vec4(normalize(r.directionStep),1); //rainbow background
			// out_Color = vec4((normalize(it)+1)/2,1);
			out_Color = vec4((it/1)+0.5,1);

			// out_Color = vec4(k, 0, 0, 1); //somehow im always here with k=0?
			// return;
		}

		// r.position += r.directionStep; //go one step
		const float c_box_BVR = 6;
		float distance = (sqrt(absEuclidSqr(it)) - c_box_BVR)/abs(g_box_DEFactor);
		r.position += r.directionNorm * distance;
		// break;
	}

	// if we dont hit anything
	// out_Color = vec4(0,0,0,1);
	// out_Color = vec4(normalize(r.directionStep),1); //rainbow background
	// out_Color = vec4( (var_rayWaypoint.xyz + vec3(1,1,0))/2,  1);
	// out_Color = vec4(r.position/20+vec3(0.5,0.5,0), 1);
}
